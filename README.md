# Credit card sized COVID-Certificate in LaTeX

A LaTeX Template to make a credit card sized COVID-certificate that you can stick on the back of your ID. This is a German version but it can be adapted to your language. There are a few different language versions in the [src](./src/) folder, including English/German and English/French.

## Example

![The front of the card](./covid-cert.png)

This image was made using [`covid-cert-de-en-template.tex`](https://gitlab.com/matthias-vogt/latex-covid-cert/-/raw/main/src/covid-cert-de-en-template.tex).

## Getting started

1. Insert your QR code, either
    - using the PDF of your EU Digital COVID Certificate which has your QR-Code,
        1. Put the PDF in the same directory as your chosen .tex file. You can get it from the *CovPass* App or from the Corona-Warn-App.
        2. Update line 30 in your chosen .tex file where it says `\newcommand{\qrCodeFileName}{} % Ex. code.png`, and insert the file name inside the second set of brackets.
    - or get your QR code as an image.
        1. Put the image in the same directory as your chosen .tex file.
        2. Update line 30 in your chosen .tex file where it says `\newcommand{\qrCodeFileName}{} % Ex. code.png`, and insert the file name inside the second set of brackets.
3. Insert the rest of your information in the block where it says `ENTER YOUR PERSONAL INFORMATION HERE` between lines 18 and 30. If you put the QR code file in the same directory as this file, you do not need to change line 29 where it says `\newcommand{\qrCodeFilePath}{{./}} % Default: Same path as this .tex file`.
4. Run `xelatex <filename>.tex` in this directory on your Terminal. If you don't have LaTeX installed, you can [refer to this installation page](https://www.latex-project.org/get/).
5. Print out the PDF, cut it, and laminate it matte. It's super nice.
6. Tape the card to the back of your ID for easy checking.

## Contributing
Contributions are welcome :)

## License
This project is licensed under the MIT license.
